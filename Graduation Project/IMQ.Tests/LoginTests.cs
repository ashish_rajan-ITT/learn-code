using System;
using Xunit;
using IMQ.Services;
using Moq;
using IMQ.Models;
using IMQ.Tests;
using IMQ.Repositories;

namespace IMQ.Login.Tests
{
    public class LoginTests
    {
        [Fact]
        public void verifyClient_shouldVerifyPublisher_shouldPass()
        {
            var mockRepo = new Mock<IClientRepository>();
            var req = new LoginRequest();
            req.role = Role.Publisher;
            req.clientName = "test";
            req.clientSecretKey="test123";
            mockRepo.Setup(x => x.verifyPublisher(req.clientName, req.clientSecretKey));
            var loginService = new LoginService(mockRepo.Object);
            loginService.verifyClient(req);

            mockRepo.Verify(x => x.verifyPublisher(req.clientName, req.clientSecretKey), Times.Exactly(1));
        }

        [Fact]
        public void verifyClient_shouldVerifySubscriber_shouldPass()
        {
            var mockRepo = new Mock<IClientRepository>();
            var req = new LoginRequest();
            req.role = Role.Subscriber;
            req.clientName = "test";
            req.clientSecretKey="test123";
            mockRepo.Setup(x => x.verifyPublisher(req.clientName, req.clientSecretKey));
            var loginService = new LoginService(mockRepo.Object);
            loginService.verifyClient(req);

            mockRepo.Verify(x => x.verifySubscriber(req.clientName, req.clientSecretKey), Times.Exactly(1));
        }
    }
}
