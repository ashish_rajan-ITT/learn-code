using System;
using Xunit;
using IMQ.Services;
using Moq;
using IMQ.Models;
using IMQ.Tests;
using IMQ.Repositories;

namespace IMQ.Publisher.Tests
{
    public class PublisherTests
    {
        [Fact]
        public void publishMessage_shouldCallRepo_shouldPass()
        {
            var mockRepo = new Mock<IClientRepository>();
            var req = new Request();
            req.topic = "test";
            req.message = "test_message";
            mockRepo.Setup(x => x.savePublisherMessage(req.topic, req.message));
            var pubService = new PublisherService(mockRepo.Object);
            pubService.publishMessage(req);

            mockRepo.Verify(x => x.savePublisherMessage(req.topic, req.message), Times.Exactly(1));
        }

        [Fact]
        public void getAllTopics_ReturnsTopicList_shouldPass()
        {
            var mockRepo = new Mock<IClientRepository>();
            mockRepo.Setup(x => x.getAllTopics())
            .Returns(TestData.getAllTopics());
            var pubService = new PublisherService(mockRepo.Object);
            var req = new Request();
            req.topic = "test";
            var actual = pubService.getAllTopics();
            var expected = TestData.getAllTopics();

            Assert.True(actual != null);
            Assert.Equal(actual, expected);
            mockRepo.Verify(x => x.getAllTopics(), Times.Exactly(1));
        }

         [Theory]
         [InlineData("test1")]
         [InlineData("test2")]
         [InlineData("test3")]
        public void createTopic_shouldCallRepo_shouldPass(string topic)
        {
            var mockRepo = new Mock<IClientRepository>();
            mockRepo.Setup(x => x.createTopic(topic));
            var pubService = new PublisherService(mockRepo.Object);
            pubService.createTopic(topic);

            mockRepo.Verify(x => x.createTopic(topic), Times.Exactly(1));
        }
    }
}
