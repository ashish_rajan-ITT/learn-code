using System.Collections.Generic;
using IMQ.Models;

namespace IMQ.Tests
{
    public static class TestData{

        public static List<string> getAllTopics(){
            return new List<string>(){"test", "test1", "test2"};
        }
        public static List<string> getSubscribedTopics(){
            return new List<string>(){"test", "test1"};
        }

        public static Queue<MessageView> getAllSubscriberMessages(){
            var testQueue = new Queue<MessageView>();
            var msg = new MessageView(); msg.topic = "testTopic"; msg.message = "testMessage";
            var msg2 = new MessageView(); msg2.topic = "testTopic2"; msg2.message = "testMessage2";
            var msg3 = new MessageView(); msg2.topic = "testTopic3"; msg2.message = "testMessage3";

            testQueue.Enqueue(msg);
            testQueue.Enqueue(msg2);
            testQueue.Enqueue(msg3);
            return testQueue;
        }
    }


}