using System;
using Xunit;
using IMQ.Services;
using Moq;
using IMQ.Models;
using IMQ.Tests;
using IMQ.Repositories;

namespace IMQ.Subscriber.Tests
{
    public class SubscriberTests
    {
        [Fact]
        public void getSubscribedTopics_shouldReturnSubscribedTopics_shouldPass()
        {
            var mockRepo = new Mock<IClientRepository>();
            string clientName = "test";
            mockRepo.Setup(x => x.getSubscribedTopics(clientName))
            .Returns(TestData.getSubscribedTopics());
            var subService = new SubscriberService(mockRepo.Object);
            subService.getSubscribedTopics(clientName);
            var actual = subService.getSubscribedTopics(clientName);
            var expected = TestData.getSubscribedTopics();

            Assert.True(actual != null);
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void pullSubscribedMessages_shouldPass()
        {
            var mockRepo = new Mock<IClientRepository>();
            string clientName = "testClient";
            mockRepo.Setup(x => x.pullSubscribedMessages(clientName))
            .Returns(TestData.getAllSubscriberMessages());
            var subService = new SubscriberService(mockRepo.Object);
            var actual = subService.pullSubscribedMessages(clientName);
            var expected = TestData.getAllSubscriberMessages();

            Assert.True(actual != null);
            mockRepo.Verify(x => x.pullSubscribedMessages(clientName), Times.Exactly(1));
            for(int i=0; i<= actual.Count; i++){
                var actualMsg = actual.Dequeue();
                var expectedMsg = expected.Dequeue();

                Assert.Equal(actualMsg.topic,expectedMsg.topic);
                Assert.Equal(actualMsg.message,expectedMsg.message);
            }
        }

         [Theory]
         [InlineData("test1","topic1")]
         [InlineData("test2","topic2")]
         [InlineData("test3","topic3")]
        public void subscribeTopic_ShouldCall(string clientName, string topic)
        {
            var mockRepo = new Mock<IClientRepository>();
            mockRepo.Setup(x => x.subscribeToTopic(clientName, topic));
            var subService = new SubscriberService(mockRepo.Object);
            subService.subscribeTopic(clientName, topic);

            mockRepo.Verify(x => x.subscribeToTopic(topic, clientName), Times.Exactly(1));
        }

         [Theory]
         [InlineData("test1")]
         [InlineData("test2")]
         [InlineData("test3")]
        public void getAllTopicsList_ReturnsTopicList_shouldPass(string topic)
        {
            var mockRepo = new Mock<IClientRepository>();
            mockRepo.Setup(x => x.getAllTopics())
            .Returns(TestData.getAllTopics());
            var subService = new SubscriberService(mockRepo.Object);
            var req = new Request();
            req.topic = topic;
            var actual = subService.getAllTopics();
            var expected = TestData.getAllTopics();

            Assert.True(actual != null);
            Assert.Equal(actual, expected);
            mockRepo.Verify(x => x.getAllTopics(), Times.Exactly(1));
        }
    }
}
