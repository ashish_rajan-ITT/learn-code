using System.Diagnostics;
using System;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Runtime.InteropServices;
using IMQ.Models;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Client
{
    class ClientService
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private PublisherService publisher;
        private SubscriberService subscriber;
        private IConfiguration config;
        private bool isLoggedIn;
        Role role;
        string clientName;
        public ClientService()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            this.config = builder.Build();
            string ipAddress = config["ServerAddress:IPAddress"];
            Int32 portNumber = int.Parse(config["ServerAddress:Port"]);
            this.publisher = new PublisherService();
            this.subscriber = new SubscriberService();
            clientName = config["clientDetails:name"];
            isLoggedIn = false;
            client = new TcpClient(ipAddress, portNumber);
            initializeRole();
            createStreams();
        }
        public void connectToServer()
        {
            try
            {
                bool isLoggedIn = sendLoginRequest();
                if (isLoggedIn)
                {
                    while (true)
                    {
                        if (role == Role.Publisher)
                        {
                            publisher.communicateToServer(streamReader, streamWriter);
                        }
                        if (role == Role.Subscriber)
                        {
                            subscriber.communicateToServer(streamReader, streamWriter);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e);
            }
            finally{
                closeConnection();
            }
        }
        private void initializeRole()
        {
            var clientRole = config["clientDetails:role"];
            if (clientRole == "publisher")
            {
                this.role = Role.Publisher;
            }
            if (clientRole == "subscriber")
            {
                this.role = Role.Subscriber;
            }
        }
        private void createStreams()
        {
            stream = client.GetStream();
            streamReader = new StreamReader(stream);
            streamWriter = new StreamWriter(stream);
        }

        private bool sendLoginRequest()
        {
            LoginRequest loginRequest = new LoginRequest();
            loginRequest.clientName = config["clientDetails:name"];
            loginRequest.clientSecretKey = config["clientDetails:secretKey"];
            loginRequest.role = this.role;
            streamWriter.WriteLine(JsonConvert.SerializeObject(loginRequest));
            streamWriter.Flush();
            return receiveLoginAck();
        }
        private bool receiveLoginAck()
        {
            LoginResponse loginResponse = JsonConvert.DeserializeObject<LoginResponse>(streamReader.ReadLine());
            string receivedMessage = loginResponse.message;
            bool loginSuccess = loginResponse.loginSuccess;
            if (loginSuccess)
            {
                System.Console.WriteLine("Login Successful");
            }
            else
            {
                System.Console.WriteLine(receivedMessage);
            }
            return loginSuccess;
        }
        public void closeConnection()
        {
            stream.Close();
            client.Close();
        }
    }
}
