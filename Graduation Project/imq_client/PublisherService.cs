using System.Diagnostics;
using System;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Runtime.InteropServices;
using IMQ.Models;
using IMQ.Commands;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Client
{
    class PublisherService
    {

        private StreamReader streamReader;
        private IConfiguration config;
        private StreamWriter streamWriter;

        public PublisherService()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            this.config = builder.Build();
        }

        public void communicateToServer(StreamReader _streamReader, StreamWriter _streamWriter)
        {
            this.streamReader = _streamReader;
            this.streamWriter = _streamWriter;
            try
            {
                showAllTopics();

                System.Console.WriteLine("1.Publish Message for a topic(cmd: publish) or 2.Create a new topic for publishing (cmd: createTopic)");
                var options = CommandReader.readCommand(Role.Publisher);
                switch (options)
                {
                    case 1:
                        this.publishMessage();
                        break;
                    case 2:
                        this.createNewTopic();
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }
        private List<string> getAllTopics()
        {
            getAllTopicsFromIMQ();
            List<string> receivedTopics = receiveTopicsFromServer();
            return receivedTopics;
        }
        private void showAllTopics()
        {
            List<string> alltopics = this.getAllTopics();
            System.Console.WriteLine("Available Topics are: ");
            foreach (var topic in alltopics)
            {
                Console.WriteLine(topic);
            }
        }
        private void getAllTopicsFromIMQ()
        {
            Request request = new Request();
            request.action = Actions.getAllTopic;
            streamWriter.WriteLine(JsonConvert.SerializeObject(request));
            streamWriter.Flush();
        }
        private List<string> receiveTopicsFromServer()
        {
            List<string> receivedtopics = JsonConvert.DeserializeObject<List<string>>(streamReader.ReadLine());
            return receivedtopics;
        }
        private void publishMessage()
        {
            string topic = getTopic();
            string message = getMessage();
            sendMessage(topic, message);
            receiveAck();
        }
        private void sendMessage(string topic, string message)
        {
            Request request = new Request();
            request.action = Actions.publishMessage;
            request.message = message;
            request.topic = topic;
            request.clientName = config["clientDetails:name"];
            streamWriter.WriteLine(JsonConvert.SerializeObject(request));
            streamWriter.Flush();
            Console.WriteLine("Message Sent: {0}", message);
        }
        private string receiveAck()
        {
            string receivedMessage = streamReader.ReadLine();
            Console.WriteLine("Received message: {0}", receivedMessage);
            return receivedMessage;
        }
        private void createNewTopic()
        {
            string topic = getTopic();
            sendNewTopic(topic);
            receiveAck();
        }
        private void sendNewTopic(string topic)
        {
            Request request = new Request();
            request.action = Actions.createTopic;
            request.topic = topic;
            request.clientName = config["clientDetails:name"];
            streamWriter.WriteLine(JsonConvert.SerializeObject(request));
            streamWriter.Flush();
        }

        private string getMessage()
        {
            Console.WriteLine("Enter the message to send to server: ");
            string messageToSend = Console.ReadLine();
            return messageToSend;
        }
        private string getTopic()
        {
            Console.WriteLine("Enter the topic Name:  ");
            string topic = Console.ReadLine();
            return topic;
        }
    }
}

