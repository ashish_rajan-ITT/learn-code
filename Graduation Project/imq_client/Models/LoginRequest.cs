using System;
namespace IMQ.Models
{
    public class LoginRequest
    {
        public string clientName {get; set;}
        public string clientSecretKey{get; set;}
        public Role role{get; set;}
        public string action {get;set;}
    }
}
