using System;
namespace IMQ.Models
{
    public class Request
    {
        public string topic {get; set;}
        public string message {get; set;}
        public string clientName {get; set;}
        public Actions action {get; set;}
        public string role {get; set;}
        
    }
}
