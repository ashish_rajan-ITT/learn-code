using System;
namespace IMQ.Models
{
    public enum Actions
    {
        getAllTopic,
        publishMessage,
        createTopic,
        getSubscribedTopic,
        pullMessages,
        subscribeTopic
    }
}