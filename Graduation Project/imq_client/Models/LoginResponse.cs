using System;
namespace IMQ.Models
{
    public class LoginResponse
    {
        public bool loginSuccess {get; set;}
        public string message {get; set;}
    }
}