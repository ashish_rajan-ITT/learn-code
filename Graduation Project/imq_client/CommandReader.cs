using IMQ.Models;
using System.Linq;
using System;

namespace IMQ.Commands
{
    public static class CommandReader{
        public static int readCommand(Role role) {
            var allowedCommandsSubscriber = new string[] {"pull","subscribedtopics","subscribenewtopic"};
            var allowedCommandsPublisher = new string[] {"publish","createtopic"};
            
            string commandWhole = (Console.ReadLine()).ToLower();
            string[] command = commandWhole.Split(" ");

            if(command[0] == "imq" && role == Role.Publisher && allowedCommandsPublisher.Contains(command[1]))
                return Array.IndexOf(allowedCommandsPublisher,command[1])+1;
            if(command[0] == "imq" && role == Role.Subscriber && allowedCommandsSubscriber.Contains(command[1]))
                return Array.IndexOf(allowedCommandsSubscriber, command[1])+1;

            else{
                throw new InvalidOperationException("This Command is invalid");
            }
        }

    }
}