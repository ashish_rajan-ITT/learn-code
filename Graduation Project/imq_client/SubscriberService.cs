using System;
using System.IO;
using IMQ.Models;
using IMQ.Commands;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Client
{
    class SubscriberService
    {
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private IConfiguration config;
        string clientName;
        public SubscriberService()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            this.config = builder.Build();
            clientName = config["clientDetails:name"];
        }
        public void communicateToServer(StreamReader _streamReader, StreamWriter _streamWriter)
        {
            this.streamReader = _streamReader;
            this.streamWriter = _streamWriter;
            try
            {
                System.Console.WriteLine("1.Pull All the messages or 2.Look for all subscribed Topics (cmd: subscribedTopics) or 3.Subscribe to New Topic (cmd: subscribeNewTopic)");
                int options = CommandReader.readCommand(Role.Subscriber);
                switch (options)
                {
                    case 1:
                        this.pullAllMessages();
                        break;
                    case 2:
                        this.seeAllSubscribedTopics();
                        break;
                    case 3:
                        this.subscribeToNewTopic();
                        break;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }
        private List<string> getAllTopics()
        {
            getAllTopicsFromIMQ();
            List<string> receivedTopics = receiveTopicsFromServer();
            return receivedTopics;
        }
        private void showAllTopics()
        {
            List<string> alltopics = this.getAllTopics();
            System.Console.WriteLine("Available Topics are: ");
            foreach (var topic in alltopics)
            {
                Console.WriteLine(topic);
            }
        }
        private void getAllTopicsFromIMQ()
        {
            Request request = new Request();
            request.action = Actions.getAllTopic;
            streamWriter.WriteLine(JsonConvert.SerializeObject(request));
            streamWriter.Flush();
        }
        private List<string> receiveTopicsFromServer()
        {
            List<string> receivedtopics = JsonConvert.DeserializeObject<List<string>>(streamReader.ReadLine());
            return receivedtopics;
        }
        private string receiveAck()
        {
            string receivedMessage = streamReader.ReadLine();
            Console.WriteLine("Received message: {0}", receivedMessage);
            return receivedMessage;
        }
        private void subscribeToNewTopic()
        {
            showAllTopics();
            System.Console.WriteLine("Provide Topic To Subscribe");
            string topic = getTopic();
            Request request = new Request();
            request.topic = topic;
            request.action = Actions.subscribeTopic;
            request.clientName = this.clientName;
            streamWriter.WriteLine(JsonConvert.SerializeObject(request));
            streamWriter.Flush();
            receiveAck();
        }

        private void seeAllSubscribedTopics()
        {
            Request request = new Request();
            request.action = Actions.getSubscribedTopic;
            request.clientName = this.clientName;
            streamWriter.WriteLine(JsonConvert.SerializeObject(request));
            streamWriter.Flush();
            showSubscribedTopics();
        }
        private void showSubscribedTopics(){
            List<string> topics = JsonConvert.DeserializeObject<List<string>>(streamReader.ReadLine());
            System.Console.WriteLine("Subscribed Topics Are: ");
            foreach (string topic in topics)
            {
                System.Console.WriteLine(topic);
            }
        }
        private void pullAllMessages()
        {
            Queue<MessageView> receivedMessages = getSubscribedMessages();
            foreach (MessageView msg in receivedMessages)
            {
                System.Console.WriteLine($"Topic: {msg.topic}  Message: {msg.message}");
            }
            System.Console.WriteLine();
        }
        private Queue<MessageView> getSubscribedMessages()
        {
            Queue<MessageView> messages = new Queue<MessageView>();
            Request request = new Request();
            request.clientName = this.clientName;
            request.action = Actions.pullMessages;
            streamWriter.WriteLine(JsonConvert.SerializeObject(request));
            streamWriter.Flush();
            messages = JsonConvert.DeserializeObject<Queue<MessageView>>(streamReader.ReadLine());
            return messages;
        }
        private void subscribeTopic()
        {
            string topic = getTopic();
            sendMessageToSubscribe(topic);
            receiveAck();
        }
        private void sendMessageToSubscribe(string topic)
        {
            Request request = new Request();
            request.action = Actions.subscribeTopic;
            request.topic = topic;
            request.clientName = config["clientDetails:name"];
            streamWriter.WriteLine(JsonConvert.SerializeObject(request));
            streamWriter.Flush();
        }
        private string getTopic()
        {
            Console.WriteLine("Enter the topic Name:  ");
            string topic = Console.ReadLine();
            return topic;
        }

    }
}
