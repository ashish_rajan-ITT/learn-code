using System;
using IMQ.Models;
using System.Collections.Generic;
using IMQ.Repositories;

namespace IMQ.Services
{
    public class PublisherService : IPublisherService
    {
        private IClientRepository clientRepository;
        public PublisherService(IClientRepository _clientRepository)
        {
            clientRepository = _clientRepository;
        }
      
        public void publishMessage(Request request)
        {
            string topic = request.topic;
            string clientMessage = request.message;
            clientRepository.savePublisherMessage(topic, clientMessage);
            Console.WriteLine("Recieved Message : {0}", clientMessage);
        }
        public List<string> getAllTopics()
        {
            return clientRepository.getAllTopics();
        }
        public void createTopic(string topicName)
        {
            clientRepository.createTopic(topicName);
        }
    }
}