using IMQ.Models;
using System.Collections.Generic;
using IMQ.Repositories;
namespace IMQ.Services
{
    public class SubscriberService : ISubscriberService
    {
        private IClientRepository clientRepository;
        public SubscriberService(IClientRepository _clientRepository)
        {
            clientRepository = _clientRepository;
        }
        public List<string> getSubscribedTopics(string clientName)
        {
            return clientRepository.getSubscribedTopics(clientName);
        }
        public Queue<MessageView> pullSubscribedMessages(string client)
        {
            moveMessagesToDeadLetterQueue();
            return clientRepository.pullSubscribedMessages(client);
        }
        public void subscribeTopic(string topicName, string clientName)
        {
            clientRepository.subscribeToTopic(clientName, topicName);
        }
        public List<string> getAllTopics()
        {
            return clientRepository.getAllTopics();
        }
        private void moveMessagesToDeadLetterQueue() {
            clientRepository.moveToDeadLetterQueue();
        }
    }
}