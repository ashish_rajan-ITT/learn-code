using System;
using System.Net;
using System.Threading;
using IMQ.Controllers;
using IMQ.Exceptions;
using System.Net.Sockets;

namespace IMQ.Services
{
    class ServerService : IServerService
    {
        private TcpListener server;
        private TcpClient client;
        private string ipAddress;
        private Int32 portNumber;
        private ILoginController loginController;
        private int totalClientConnected;
        private int maxAllowedclients;
        public ServerService(ILoginController _loginController)
        {
            var serverConstants = new ServerConstants();
            this.ipAddress = serverConstants.ipAddress;
            this.portNumber = serverConstants.portNumber;
            this.totalClientConnected = 0;
            this.maxAllowedclients = 4;
            IPAddress localAddress = IPAddress.Parse(ipAddress);
            server = new TcpListener(localAddress, portNumber);
            this.loginController = _loginController;
        }
        public void startServer()
        {
            try
            {
                server.Start();
                Console.WriteLine("Server Started.....");

                while (true)
                {
                    client = server.AcceptTcpClient();

                    if (totalClientConnected <= maxAllowedclients)
                    {
                        this.totalClientConnected++;
                        Thread newThread = new Thread(connectToClient);
                        newThread.Start();
                    }
                    else
                    {
                        throw new MaxAllowedClientsException(client);
                    }
                }
            }
            catch (Exception error)
            {
                Console.WriteLine("Exception : {0}", error.Message);
            }
        }
        public void connectToClient()
        {
            Console.WriteLine("New Client Connected");
            loginController.connectToClient(client);
        }
    }
}