using IMQ.Models;
using IMQ.Repositories;

namespace IMQ.Services
{
    public class LoginService : ILoginService
    {
        private IClientRepository clientRepository;
        public LoginService(IClientRepository _clientRepository)
        {
            clientRepository = _clientRepository;
        }

        public bool verifyClient(LoginRequest request)
        {
            bool correctCredentials = false;
            string clientName = request.clientName;
            string secretKey = request.clientSecretKey;
            Role role = request.role;
            if (role == Role.Publisher)
            {
                correctCredentials = clientRepository.verifyPublisher(clientName, secretKey);
            }
            else if (role == Role.Subscriber)
            {
                correctCredentials = clientRepository.verifySubscriber(clientName, secretKey);
            }
            return correctCredentials;
        }
    }
}