using System;
using System.Net.Sockets;
using System.Collections.Generic;
using IMQ.Models;

namespace IMQ.Services
{
    public interface IPublisherService
    {
      void publishMessage(Request request);
      List<string> getAllTopics();
      void createTopic(string clientName);
        
    }
}