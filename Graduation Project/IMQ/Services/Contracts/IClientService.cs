using System;
using System.Net.Sockets;

namespace IMQ.Services
{
    public interface IClientService
    {
        void connectToClient(TcpClient _client);
        void sendAck(string receivedMsg);
        
    }
}