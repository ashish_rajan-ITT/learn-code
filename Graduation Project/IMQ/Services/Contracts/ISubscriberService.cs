using System;
using System.Net.Sockets;
using System.Collections.Generic;
using IMQ.Models;

namespace IMQ.Services
{
    public interface ISubscriberService
    {
        List<string> getSubscribedTopics(string clientName);
        Queue<MessageView> pullSubscribedMessages(string client);
        void subscribeTopic(string topicName, string clientName);
        List<string> getAllTopics();
    }
}