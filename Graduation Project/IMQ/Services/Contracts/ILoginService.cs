using System;
using System.Net.Sockets;
using System.Collections.Generic;
using IMQ.Models;

namespace IMQ.Services
{
    public interface ILoginService
    {
        bool verifyClient(LoginRequest request);
    }
}