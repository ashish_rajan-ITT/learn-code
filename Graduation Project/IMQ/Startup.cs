using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IMQ.Services;
using IMQ.Repositories;
using IMQ.Controllers;
using Microsoft.Extensions.Hosting;


namespace IMQ
{
    public class Startup
    {
        private static IConfigurationBuilder _builder { get; set; }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
        .ConfigureServices((_, services) =>
        {
            services.AddScoped<IServerService, ServerService>();
            services.AddScoped<IPublisherService, PublisherService>();
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<ISubscriberService, SubscriberService>();
            services.AddScoped<ILoginController, LoginController>();
            services.AddScoped<IPublisherController, PublisherController>();
            services.AddScoped<ISubscriberController, SubscriberController>();
            services.AddScoped<IClientRepository, ClientRepository>();
        });
        
        public static void BuildConfig(IConfigurationBuilder builder)
        {
            builder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                _builder = builder;
        }
    }
}