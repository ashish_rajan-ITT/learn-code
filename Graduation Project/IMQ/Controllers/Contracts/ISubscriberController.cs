using System;
using System.IO;
using System.Net.Sockets;

namespace IMQ.Controllers
{
    public interface ISubscriberController
    {
     public void communicateWithSubscriber(StreamReader _streamReader, StreamWriter _streamWriter);
    }
}