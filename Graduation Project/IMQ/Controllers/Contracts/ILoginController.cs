using System;
using System.Net.Sockets;

namespace IMQ.Controllers
{
    public interface ILoginController
    {
        void connectToClient(TcpClient _client);
    }
}