using System;
using System.IO;
using System.Net.Sockets;

namespace IMQ.Controllers
{
    public interface IPublisherController
    {
     public void communicateWithPublisher(StreamReader _streamReader, StreamWriter _streamWriter);
    }
}