using System.IO;
using System;
using IMQ.Models;
using IMQ.Services;
using System.Net.Sockets;
using Newtonsoft.Json;
using IMQ.Exceptions;
namespace IMQ.Controllers
{
    public class LoginController : ILoginController
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private IPublisherController publisherController;
        private ISubscriberController subscriberController;
        private ILoginService loginService;
        private Role clientRole;
        public LoginController(ILoginService _loginService, IPublisherController _publisherController, ISubscriberController _subscriberController)
        {
            loginService = _loginService;
            publisherController = _publisherController;
            subscriberController = _subscriberController;
        }
        public void connectToClient(TcpClient _client)
        {
            try
            {
                client = _client;
                initializeStreams();

                AuthenticateClient();

                if (this.clientRole == Role.Publisher)
                {
                    publisherController.communicateWithPublisher(streamReader, streamWriter);
                }
                if (this.clientRole == Role.Subscriber)
                {
                    subscriberController.communicateWithSubscriber(streamReader, streamWriter);
                }
            }
            catch (ClientDoesNotExistsException e)
            {
                System.Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
            }
        }

        private void AuthenticateClient()
        {
            LoginRequest newLoginRequest = JsonConvert.DeserializeObject<LoginRequest>(streamReader.ReadLine());
            bool clientVerified = loginService.verifyClient(newLoginRequest);
            if (clientVerified)
                sendLoginAck(newLoginRequest.clientName, true);
            else
            {
                sendLoginAck(newLoginRequest.clientName, false);
                throw new ClientDoesNotExistsException();
            }
            this.clientRole = newLoginRequest.role;
        }
        private void initializeStreams()
        {
            stream = client.GetStream();
            streamReader = new StreamReader(stream);
            streamWriter = new StreamWriter(stream);
        }
        private void sendAck(string msg)
        {
            streamWriter.WriteLine(msg);
            streamWriter.Flush();
        }
        private void sendLoginAck(string clientName, bool loginSuccess)
        {
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.loginSuccess = loginSuccess;
            loginResponse.message = clientName;
            streamWriter.WriteLine(JsonConvert.SerializeObject(loginResponse));
            streamWriter.Flush();
        }

    }
}