using System;
using System.IO;
using IMQ.Models;
using IMQ.Services;
using System.Collections.Generic;
using IMQ.Repositories;
using IMQ.Exceptions;
using Newtonsoft.Json;
namespace IMQ.Controllers
{
    public class SubscriberController : ISubscriberController
    {
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private ISubscriberService subscriberService;
        public SubscriberController(ISubscriberService _subscriberService)
        {
            subscriberService = _subscriberService;
        }
        public void communicateWithSubscriber(StreamReader _streamReader, StreamWriter _streamWriter)
        {
            this.streamReader = _streamReader;
            this.streamWriter = _streamWriter;

            while (true)
            {
                try
                {
                    Request newRequest = readRequest();
                    switch (newRequest.action)
                    {
                        case Actions.getAllTopic:
                            this.sendAllTopics(newRequest);
                            break;

                        case Actions.getSubscribedTopic:
                            this.getSubscribedTopics(newRequest);
                            break;

                        case Actions.pullMessages:
                            this.pullMessages(newRequest);
                            break;
                        case Actions.subscribeTopic:
                            this.subscribeTopic(newRequest);
                            break;
                    }
                }
                catch (ClientDisconnectedException e)
                {
                    System.Console.WriteLine(e.Message);
                }
                catch (Exception error)
                {
                    Console.WriteLine("Exception {0}", error.Message);
                }
                finally
                {
                    streamWriter.Flush();
                }
            }
        }
        private void getSubscribedTopics(Request request)
        {
            string clientName = request.clientName;
            List<string> topics = subscriberService.getSubscribedTopics(clientName);
            streamWriter.WriteLine(JsonConvert.SerializeObject(topics));
            streamWriter.Flush();
        }
        private Request readRequest()
        {
            Request newRequest = JsonConvert.DeserializeObject<Request>(streamReader.ReadLine());
            return newRequest;
        }
        private void pullMessages(Request request)
        {
            string client = request.clientName;
            Queue<MessageView> messages = subscriberService.pullSubscribedMessages(client);
            streamWriter.WriteLine(JsonConvert.SerializeObject(messages));
            streamWriter.Flush();
        }
        private void subscribeTopic(Request request)
        {
            string topicName = request.topic;
            string clientName = request.clientName;
            subscriberService.subscribeTopic(topicName, clientName);
            sendAck("Subscribed");
        }
        private void sendAllTopics(Request request)
        {
            List<string> topics = subscriberService.getAllTopics();
            streamWriter.WriteLine(JsonConvert.SerializeObject(topics));
            streamWriter.Flush();
        }
        private void sendAck(string receivedMsg)
        {
            streamWriter.WriteLine(receivedMsg);
            streamWriter.Flush();
        }

    }
}