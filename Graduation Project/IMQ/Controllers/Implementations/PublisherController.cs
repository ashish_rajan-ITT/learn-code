using System;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using IMQ.Models;
using System.Net.Sockets;
using System.Collections.Generic;
using IMQ.Repositories;
using IMQ.Services;
using IMQ.Exceptions;
using Newtonsoft.Json;
namespace IMQ.Controllers
{
    public class PublisherController : IPublisherController
    {
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private IPublisherService publisherService;
        public PublisherController( IPublisherService _publisherService)
        {
            publisherService = _publisherService;
        }
        public void communicateWithPublisher(StreamReader _streamReader, StreamWriter _streamWriter)
        {
            this.streamReader = _streamReader;
            this.streamWriter = _streamWriter;
    
            while (true)
            {
                try
                {
                    Request newRequest = readRequest();
                    switch (newRequest.action)
                    {
                        case Actions.getAllTopic:
                            this.sendAllTopics();
                            break;
                        case Actions.publishMessage:
                            this.publishMessage(newRequest);
                            break;
                        case Actions.createTopic:
                            this.createTopic(newRequest);
                            break;
                    }
                }
                catch(ClientDisconnectedException e){
                    System.Console.WriteLine(e.Message);
                }
                catch (Exception error)
                {
                    Console.WriteLine("Exception {0}", error.Message);
                }
                finally
                {
                    streamWriter.Flush();
                }
            }
        }
        private void publishMessage(Request request)
        {
            publisherService.publishMessage(request);
            sendAck("published");
        }
        private Request readRequest(){
            Request newRequest = JsonConvert.DeserializeObject<Request>(streamReader.ReadLine());
            return newRequest;
        }
        private void sendAllTopics()
        {
            List<string> topics = publisherService.getAllTopics();
            streamWriter.WriteLine(JsonConvert.SerializeObject(topics));
            streamWriter.Flush();
        }
        private void createTopic(Request request)
        {
            string topicName = request.topic;
            publisherService.createTopic(topicName);
            sendAck("New Topic Created");
        }
        private void sendAck(string receivedMsg)
        {
            streamWriter.WriteLine(receivedMsg);
            streamWriter.Flush();
        }
    }
}