using System;
namespace IMQ.Exceptions
{
    public class ClientDisconnectedException : System.IO.IOException
    {
        public ClientDisconnectedException() : base("Client Disconnected Unexpectedly")
        {
        }
    }
     public class ClientDoesNotExistsException : Exception
    {
        public ClientDoesNotExistsException() : base("Client Does Not Exists")
        {
        }
    }
}
