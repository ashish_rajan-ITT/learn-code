using System.Net.Sockets;
using System;
using System.IO;
using Newtonsoft.Json;
using IMQ.Models;
namespace IMQ.Exceptions
{
    public class MaxAllowedClientsException : Exception
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        public MaxAllowedClientsException(TcpClient _client) : base("Maximum client numbers reached")
        {
            client = _client;
            initializeStreams();
            sendWaitMessageToClient();
        }
        private void initializeStreams()
        {
            stream = client.GetStream();
            streamReader = new StreamReader(stream);
            streamWriter = new StreamWriter(stream);
        }
        private void sendWaitMessageToClient(){
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.loginSuccess = false;
            loginResponse.message = "Server is busy, Please connect after some time";
            streamWriter.WriteLine(JsonConvert.SerializeObject(loginResponse));
            streamWriter.Flush();
        }
    }


}