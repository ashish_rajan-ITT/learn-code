using System;
namespace IMQ.Exceptions
{
    public class InvalidTopicException : Exception
    {
        public InvalidTopicException()
            : base("Invalid Topic Name")
        {
        }
    }
    public class TopicAlreadyExists : Exception
    {
        public TopicAlreadyExists()
            : base("This Topic Already Exists")
        {
        }
    }
}
