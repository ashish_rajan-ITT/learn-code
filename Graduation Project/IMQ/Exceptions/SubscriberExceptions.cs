using System;
namespace IMQ.Exceptions
{
    public class CantSubscribeTopicException : Exception
    {
        public CantSubscribeTopicException()
            : base("Can not subscribe to topic")
        {
        }
    }
    public class CantGetSubscribedTopics : Exception
    {
        public CantGetSubscribedTopics()
            : base("Can not fetch subscribed Topics or record does not exists")
        {
        }
    }
}
