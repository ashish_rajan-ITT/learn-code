﻿using System;
using System.Net;
using IMQ.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
namespace IMQ
{
    class IMQ
    {
        static void Main(string[] args)
        {    
            var builder = new ConfigurationBuilder();
            Startup.BuildConfig(builder);

            var host = Startup.CreateHostBuilder(args).Build();

            var imqServer = ActivatorUtilities.CreateInstance<ServerService>(host.Services);
            imqServer.startServer();
        }
    }
}
