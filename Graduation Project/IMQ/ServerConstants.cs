using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace IMQ {
    public class ServerConstants {
      public ServerConstants () {
      var builder = new ConfigurationBuilder ();
      builder.SetBasePath (Directory.GetCurrentDirectory ())
        .AddJsonFile ("appsettings.json", optional : false, reloadOnChange : true);
      IConfiguration config = builder.Build ();
      this.ipAddress = config["ServerConfig:IPAddress"];
      this.portNumber = int.Parse(config["ServerConfig:Port"]);
      this.connectionString = config["ConnectionStrings:DbConnection"];
    }
    public String ipAddress;
    public Int32 portNumber;
    public string connectionString;
  
    }
}