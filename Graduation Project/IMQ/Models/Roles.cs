using System;
namespace IMQ.Models
{
    public enum Role
    {
        Publisher,
        Subscriber
    }
}