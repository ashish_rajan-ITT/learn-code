using System;
using System.Collections.Generic;
using IMQ.Models;

namespace IMQ.Repositories
{
    public interface IClientRepository
    {
        bool checkIfClientExist(string clientName);
        void saveClientMessage (string clientName, string message, DateTime time);
        void initializeDbConnection();
        bool verifyPublisher(string publisherName, string secretKey);
        void savePublisherMessage(string topic, string message);
        List<string> getAllTopics();
        void subscribeToTopic(string clientName, string topicName);
        List<string> getSubscribedTopics(string clientName);
        List<string> pullMessages(string topic, string clientName);
        void createTopic(string topicName);
        bool verifySubscriber(string subscriberName, string secretKey);
        Queue<MessageView> pullSubscribedMessages(string name);
        void moveToDeadLetterQueue();

}
}