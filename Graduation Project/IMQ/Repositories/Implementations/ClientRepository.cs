﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using IMQ.Exceptions;
using IMQ.Models;

namespace IMQ.Repositories
{
    public class ClientRepository : IClientRepository
    {
        private SqlConnection DbConnection;
        public ClientRepository()
        {
            initializeDbConnection();
        }
        public void initializeDbConnection()
        {
            var serverConstants = new ServerConstants();
            string connectionString = serverConstants.connectionString;
            this.DbConnection = new SqlConnection(connectionString);
            DbConnection.Open();
        }
        public bool checkIfClientExist(string clientName)
        {
            string queryString = DbQueries.checkIfClientExist;
            SqlCommand command = new SqlCommand(queryString, DbConnection);
            command.Parameters.AddWithValue("@ClientName", clientName);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read() && reader["ClientName"] != null)
            {
                reader.Close();
                return true;
            }
            else
            {
                reader.Close();
                return false;
            }
        }
        public bool verifyPublisher(string publisherName, string secretKey)
        {
            string query = DbQueries.getPublisherSecretKey;
            string publisherSecretKey = "";
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.Parameters.AddWithValue("@publisherName", publisherName);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                publisherSecretKey = ((string)reader["SecretKey"]);
            }
            reader.Close();
            if (publisherSecretKey == secretKey)
                return true;

            return false;
        }
        public bool verifySubscriber(string subscriberName, string secretKey)
        {
            string query = DbQueries.getSubscriberSecretKey;
            string subscriberSecretKey = "";
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.Parameters.AddWithValue("@subscriberName", subscriberName);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                subscriberSecretKey = ((string)reader["SecretKey"]);
            }
            reader.Close();
            if (subscriberSecretKey == secretKey)
                return true;

            return false;
        }

        public void saveClientMessage(string clientName, string message, DateTime time)
        {
            string query = DbQueries.saveClientMessage(clientName);
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.Parameters.AddWithValue("@message", message);
            command.Parameters.AddWithValue("@time", time);
            command.ExecuteNonQuery();
        }
        public Queue<MessageView> pullSubscribedMessages(string name)
        {
            int subscriberId = getSubscriberId(name);
            List<int> subscribedTopics = getSubscribedTopicIds(subscriberId);
            Queue<MessageView> messages = new Queue<MessageView>();
            foreach (int topicId in subscribedTopics)
            {
                string query = DbQueries.getMessageWithTopicId;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@topicId", topicId);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    string topicName = getTopicName(topicId);
                    string message = ((string)reader["message"]);
                    MessageView newMessage = new MessageView(); 
                    newMessage.message = message;
                    newMessage.topic = topicName;
                    messages.Enqueue(newMessage);
                }
            }
            return messages;
        }
        private List<int> getSubscribedTopicIds(int subscriberId)
        {
            List<int> topicIds = new List<int>();
            string query = DbQueries.getSubscribedTopicIds;
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.Parameters.AddWithValue("@subscriberId", subscriberId);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                topicIds.Add((int)reader["topicId"]);
            }
            return topicIds;
        }
        private int getTopicId(string topic)
        {
            string query = DbQueries.getTopicId;
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.Parameters.AddWithValue("@topic", topic);
            SqlDataReader reader = command.ExecuteReader();
            int topicId = 0;
            while (reader.Read())
            {
                topicId = ((int)reader["TopicId"]);
            }
            reader.Close();
            return topicId;
        }
        private string getTopicName(int topicId){
            string topicName = "";
            string query = DbQueries.getTopicName;
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.Parameters.AddWithValue("@topicId", topicId);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                topicName = (string)reader["TopicName"];
            }
            return topicName;
        }
        private int getPublisherId(string name)
        {
            string query = DbQueries.getPublisherId;
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.Parameters.AddWithValue("@name", name);
            SqlDataReader reader = command.ExecuteReader();
            int publisherId = 0;
            while (reader.Read())
            {
                publisherId = ((int)reader["Id"]);
            }
            reader.Close();
            return publisherId;
        }
        private int getSubscriberId(string name)
        {
            string query = DbQueries.getSubscriberId;
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.Parameters.AddWithValue("@name", name);
            SqlDataReader reader = command.ExecuteReader();
            int subscriberId = 0;
            while (reader.Read())
            {
                subscriberId = ((int)reader["Id"]);
            }
            reader.Close();
            return subscriberId;
        }
        public List<string> getAllTopics()
        {
            string query = DbQueries.getAllTopics;
            SqlCommand command = new SqlCommand(query, DbConnection);
            SqlDataReader reader = command.ExecuteReader();
            List<string> topics = new List<string>();
            while (reader.Read())
            {
                topics.Add((string)reader["TopicName"]);
            }
            reader.Close();
            return topics;
        }
        public void savePublisherMessage(string topic, string message)
        {
            int topicId = getTopicId(topic);
            if(topicId == 0){
                throw new InvalidTopicException();
            }
            var time = DateTime.Now;
            string query = DbQueries.savePublisherMessage;
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.Parameters.AddWithValue("@message", message);
            command.Parameters.AddWithValue("@topicId", topicId);
            command.Parameters.AddWithValue("@time", time);
            command.ExecuteNonQuery();
        }
        public List<string> getSubscribedTopics(string clientName)
        {
            try{
            int subscriberId = getSubscriberId(clientName);
            List<int> topicsIds = getTopicIdsFromSubscriberTopicMapping(subscriberId);
            List<string> topics = new List<string>();
            foreach (int topicId in topicsIds)
            {
                string query = DbQueries.getSubscribedTopics;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@topicId", topicId);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    topics.Add((string)reader["TopicName"]);
                }
                reader.Close();
            }
            return topics;
            }
            catch{
                throw new CantGetSubscribedTopics();
            }
        }
        public List<int> getTopicIdsFromSubscriberTopicMapping(int subscriberId)
        {
            string query = DbQueries.getTopicIdsFromSubscriberTopicMapping;
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.Parameters.AddWithValue("@subscriberId", subscriberId);
            SqlDataReader reader = command.ExecuteReader();
            List<int> topicsIds = new List<int>();
            while (reader.Read())
            {
                topicsIds.Add((int)reader["topicId"]);
            }
            reader.Close();
            return topicsIds;
        }
        public List<string> pullMessages(string topic, string clientName)
        {
            int topicId = getTopicId(topic);
            int subscriberId = getSubscriberId(clientName);
            string query = DbQueries.pullMessages;
            SqlCommand command = new SqlCommand(query, DbConnection);
            SqlDataReader reader = command.ExecuteReader();
            List<string> messages = new List<string>();
            while (reader.Read())
            {
                messages.Add((string)reader["TopicName"]);
            }
            reader.Close();
            return messages;
        }
        public void subscribeToTopic(string clientName, string topicName)
        {
            try{
            int topicId = getTopicId(topicName);
            int subscriberId = getSubscriberId(clientName);
            string query = DbQueries.subscribeToTopic;
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.Parameters.AddWithValue("@topicId", topicId);
            command.Parameters.AddWithValue("@subscriberId", subscriberId);
            command.ExecuteNonQuery();
            }
            catch{
                throw new CantSubscribeTopicException();
            }
        }
        public void createTopic(string topicName)
        {
            string query = DbQueries.createTopic;
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.Parameters.AddWithValue("@topicName", topicName);
            command.ExecuteNonQuery();
        }
        private void deleteDeadMessages() {
            string query = DbQueries.deleteDeadMessages;
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.ExecuteNonQuery();
        }
        public void moveToDeadLetterQueue(){
            string query = DbQueries.moveToDeadLetterQueue;
            SqlCommand command = new SqlCommand(query, DbConnection);
            command.ExecuteNonQuery();
            deleteDeadMessages();
        }
    }
}