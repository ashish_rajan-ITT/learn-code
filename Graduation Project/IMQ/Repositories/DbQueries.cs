using System;
using System.Data.SqlClient;

namespace IMQ.Repositories
 {
    public static class DbQueries{
        public const string addToRootTable = "INSERT INTO RootTable (ClientName) VALUES (@clientName)";
        public const string checkIfClientExist = "SELECT ClientName FROM  [dbo].[RootTable] where ClientName = @clientName";
        public const string getSubscriberSecretKey = "SELECT SecretKey FROM  [dbo].[Subscriber] where name= @subscriberName";
        public const string getMessageWithTopicId = "SELECT message FROM [dbo].[Messages] WHERE topicId = @topicId";
        public const string getSubscribedTopicIds="SELECT topicId FROM [dbo].[SubscribedTopicMap] WHERE subscriberId = @subscriberId";
        public const string getPublisherSecretKey = "SELECT SecretKey FROM  [dbo].[Publishers] where PublisherName= @publisherName";
        public const string getTopicId = "SELECT TopicId from Topics WHERE TopicName = @topic";
        public const string getTopicName = "SELECT TopicName FROM [dbo].[Topics] WHERE TopicId = @topicId";
        public const string getPublisherId = "SELECT Id from Publisher WHERE name = @name";
        public const string getSubscriberId = "SELECT Id from Subscriber WHERE name = @name";
        public const string getAllTopics = "SELECT * from Topics";
        public const string moveToDeadLetterQueue = "INSERT INTO DeadLetterQueue SELECT Message, TopicId, timeStamp from Messages WHERE Messages.timeStamp < DATEADD(dd,-7,GETDATE())";
        public const string deleteDeadMessages = "DELETE FROM Messages WHERE Messages.timeStamp < DATEADD(dd,-7,GETDATE())";
        public const string savePublisherMessage= "INSERT INTO Messages (message, topicId, timeStamp) VALUES (@message, @topicId, @time)";
        public const string getSubscribedTopics = "SELECT TopicName from Topics WHERE TopicId = @topicId";
        public const string getTopicIdsFromSubscriberTopicMapping = "SELECT topicId from [dbo].[SubscribedTopicMap] WHERE subscriberId = @subscriberId";
        public const string pullMessages = "SELECT message from Messages WHERE topicId = @topicId";
        public const string createTopic = "INSERT INTO Topics (topicName) VALUES (@topicName)";
        public const string subscribeToTopic = "INSERT INTO SubscribedTopicMap (topicId, subscriberId) VALUES (@topicId, @subscriberId)";
        public static String createClientTable(String clientName){
            return "IF NOT EXISTS(SELECT NULL FROM sys.tables WHERE name='" + clientName + "') BEGIN CREATE TABLE " + clientName + " (Id int NOT NULL identity(1,1), Message varchar(50),time datetime) END";
        }
        public static String saveClientMessage(String clientName){
            return "INSERT INTO " + clientName + " (Message, time) VALUES (@message, @time)";
        }
        
        
    }
    
}