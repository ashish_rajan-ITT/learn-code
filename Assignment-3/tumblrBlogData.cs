using System.Net;
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TumblrBlogs
{
    class BlogData
    {
       
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the blog name you want to search");
            var blogName = Console.ReadLine();

            Console.WriteLine("Enter the post range");
            string postRange = Console.ReadLine();

            MakeBatch(blogName, postRange);

        }

        public static void MakeBatch(string blogName, string postRange)
        {

            //splitting the postRange into two separate array element for two or three digit integers
            string[] rangeValues = postRange.Split("-");
            try
            {

                int startRange = Int32.Parse(rangeValues[0]);
                int endRange = Int32.Parse(rangeValues[1]);

                int numberOfBatches = (endRange - startRange) / 50;

                for (int i = 0; i < numberOfBatches; i++)
                {
                    PrintBatch(blogName, startRange + (i * 50), startRange + ((i + 1) * 50));
                }

                if ((endRange - startRange) % 50 != 0)
                {
                    PrintBatch(blogName, startRange + (numberOfBatches * 50), endRange);
                }

            }
            catch (Exception e)
            {
                System.Console.WriteLine("Invalid Input");
                System.Console.WriteLine(e);
            }


        }

        public static void PrintBatch(string blogName, int startRange, int endRange)
        {

            string url = GenerateUrl(blogName, startRange, endRange);

            WebClient client = new WebClient();

            string serverResponse = client.DownloadString(url);

            string blogDetails = ExtractBlogData(serverResponse);

            DisplayBlogData(blogDetails);

        }

        public static string GenerateUrl(string blogName, int startRange, int endRange)
        {

            return ("https://" + blogName + ".tumblr.com/api/read/json?type=photo&num=" + endRange + "&start=" + startRange);

        }

        public static string ExtractBlogData(string serverResponse)
        {

            while (serverResponse.StartsWith("var tumblr_api_read = "))
            {
                serverResponse = serverResponse.Substring("var tumblr_api_read = ".Length);
            }

            serverResponse = serverResponse.Replace(";", "");

            return (serverResponse);
        }

        public static void DisplayBlogData(string blogDetails)
        {
            try
            {
                //Converting to C# .Net Object
                dynamic blogDataObject = JsonConvert.DeserializeObject<dynamic>(blogDetails);

                Console.WriteLine("title:" + blogDataObject["tumblelog"]["title"].ToString());
                Console.WriteLine("name: " + blogDataObject["tumblelog"]["name"].ToString());
                Console.WriteLine("description: " + blogDataObject["tumblelog"]["description"].ToString());
                Console.WriteLine("number of posts: " + blogDataObject["posts-total"].ToString());

                JArray imagesUrl = (JArray)blogDataObject["posts"];

                //Displaying images url
                for (int i = 0; i < imagesUrl.Count; i++)
                {
                    Console.Write(i);
                    Console.WriteLine(blogDataObject["posts"][i]["photo-url-1280"].ToString());
                }

            }
            catch (Exception e)
            { System.Console.WriteLine(e); }
        }

    }
}